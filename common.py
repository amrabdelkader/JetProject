from csv import reader
from ThreeStageCompressor import ThreeStageCompressor
from math import sqrt
import matplotlib.pyplot as plt
import warnings

with open('toBeRead.csv', 'r') as readFile:
    csvReader = reader(readFile)
    designParameters = csvReader.next()
designParameters = [float(parameter) for parameter in designParameters]
compressor = ThreeStageCompressor(27.5)
compressor.MapInletConditions(280.0, 10.0e4)

with warnings.catch_warnings():
    warnings.filterwarnings('error')
    try:
        compressor.ChangeDesignParametersAndTest(designParameters[0], designParameters[1], designParameters[2],
                                                 designParameters[3], designParameters[4], designParameters[5],
                                                 designParameters[6], designParameters[7], designParameters[8],
                                                 designParameters[9], designParameters[10], designParameters[11],
                                                 designParameters[12], designParameters[13], designParameters[14],
                                                 designParameters[15], designParameters[16], designParameters[17],
                                                 designParameters[18], designParameters[19], designParameters[20],
                                                 1, 1, 1, 1, 1, 1)

        outputFile = open('Output/Output.txt', 'w')
        outputFile.write(compressor.__str__())


        def drawTriangle(Stage, StageName):
            C1 = Stage.m_rotor.m_absVelocityInlet
            C2 = Stage.m_rotor.m_absVelocityOutlet
            W1 = Stage.m_rotor.m_relativeVelocityInlet
            W2 = Stage.m_rotor.m_relativeVelocityOutlet
            Cx1 = Stage.m_rotor.m_axialVelocityInlet
            Cx2 = Stage.m_rotor.m_axialVelocityOutlet
            Ctheta1 = sqrt(C1**2 - Cx1 ** 2)
            Wtheta1 = sqrt(W1**2 - Cx1 ** 2)
            Ctheta2 = sqrt(C2**2 - Cx2 ** 2)
            Wtheta2 = sqrt(W2**2 - Cx2 ** 2)
            plt.figure()
            plt.plot([0, 0], [0, -Cx1], 'r')
            plt.plot([0, Ctheta1], [-Cx1, -Cx1], 'r')
            plt.plot([0, Ctheta1], [0, -Cx1], 'r')
            plt.plot([0, -Wtheta1], [-Cx1, -Cx1], 'r')
            plt.plot([0, -Wtheta1], [0, -Cx1], 'r')
            plt.plot([0, 0], [0, -Cx2], 'b')
            plt.plot([0, Ctheta2], [-Cx2, -Cx2], 'b')
            plt.plot([0, Ctheta2], [0, -Cx2], 'b')
            plt.plot([0, -Wtheta2], [-Cx2, -Cx2], 'b')
            plt.plot([0, -Wtheta2], [0, -Cx2], 'b')
            plt.title(StageName)
            plt.axis('equal')
            plt.savefig('Output/'+StageName+'.jpg')

        def drawAnnulusArea(compressor):
            yCor = [compressor.m_stageOne.m_rotor.m_inletRadius,
                    compressor.m_stageOne.m_rotor.m_meanRadius,
                    compressor.m_stageOne.m_rotor.m_outletRadius,
                    compressor.m_stageOne.m_stator.m_inletRadius,
                    compressor.m_stageOne.m_stator.m_meanRadius,
                    compressor.m_stageOne.m_stator.m_outletRadius,
                    compressor.m_stageTwo.m_rotor.m_inletRadius,
                    compressor.m_stageTwo.m_rotor.m_meanRadius,
                    compressor.m_stageTwo.m_rotor.m_outletRadius,
                    compressor.m_stageTwo.m_stator.m_inletRadius,
                    compressor.m_stageTwo.m_stator.m_meanRadius,
                    compressor.m_stageTwo.m_stator.m_outletRadius,
                    compressor.m_stageThree.m_rotor.m_inletRadius,
                    compressor.m_stageThree.m_rotor.m_meanRadius,
                    compressor.m_stageThree.m_rotor.m_outletRadius,
                    compressor.m_stageThree.m_stator.m_inletRadius,
                    compressor.m_stageThree.m_stator.m_meanRadius,
                    compressor.m_stageThree.m_stator.m_outletRadius
                    ]

            zCor = [0,
                    compressor.m_stageOne.m_rotor.m_chord/2,
                    compressor.m_stageOne.m_rotor.m_chord,
                    0,
                    compressor.m_stageOne.m_stator.m_chord/2,
                    compressor.m_stageOne.m_stator.m_chord,
                    0,
                    compressor.m_stageTwo.m_rotor.m_chord / 2,
                    compressor.m_stageTwo.m_rotor.m_chord,
                    0,
                    compressor.m_stageTwo.m_stator.m_chord / 2,
                    compressor.m_stageTwo.m_stator.m_chord,
                    0,
                    compressor.m_stageThree.m_rotor.m_chord / 2,
                    compressor.m_stageThree.m_rotor.m_chord,
                    0,
                    compressor.m_stageThree.m_stator.m_chord / 2,
                    compressor.m_stageThree.m_stator.m_chord
                    ]

            rrCor = [compressor.m_stageOne.m_rotor.m_tipRadius - compressor.m_stageOne.m_rotor.m_inletHeight,
                     compressor.m_stageOne.m_rotor.m_tipRadius - compressor.m_stageOne.m_rotor.m_meanHeight,
                     compressor.m_stageOne.m_rotor.m_tipRadius - compressor.m_stageOne.m_rotor.m_outletHeight,
                     compressor.m_stageOne.m_stator.m_tipRadius - compressor.m_stageOne.m_stator.m_inletHeight,
                     compressor.m_stageOne.m_stator.m_tipRadius - compressor.m_stageOne.m_stator.m_meanHeight,
                     compressor.m_stageOne.m_stator.m_tipRadius - compressor.m_stageOne.m_stator.m_outletHeight,
                     compressor.m_stageTwo.m_rotor.m_tipRadius - compressor.m_stageTwo.m_rotor.m_inletHeight,
                     compressor.m_stageTwo.m_rotor.m_tipRadius - compressor.m_stageTwo.m_rotor.m_meanHeight,
                     compressor.m_stageTwo.m_rotor.m_tipRadius - compressor.m_stageTwo.m_rotor.m_outletHeight,
                     compressor.m_stageTwo.m_stator.m_tipRadius - compressor.m_stageTwo.m_stator.m_inletHeight,
                     compressor.m_stageTwo.m_stator.m_tipRadius - compressor.m_stageTwo.m_stator.m_meanHeight,
                     compressor.m_stageTwo.m_stator.m_tipRadius - compressor.m_stageTwo.m_stator.m_outletHeight,
                     compressor.m_stageThree.m_rotor.m_tipRadius - compressor.m_stageThree.m_rotor.m_inletHeight,
                     compressor.m_stageThree.m_rotor.m_tipRadius - compressor.m_stageThree.m_rotor.m_meanHeight,
                     compressor.m_stageThree.m_rotor.m_tipRadius - compressor.m_stageThree.m_rotor.m_outletHeight,
                     compressor.m_stageThree.m_stator.m_tipRadius - compressor.m_stageThree.m_stator.m_inletHeight,
                     compressor.m_stageThree.m_stator.m_tipRadius - compressor.m_stageThree.m_stator.m_meanHeight,
                     compressor.m_stageThree.m_stator.m_tipRadius - compressor.m_stageThree.m_stator.m_outletHeight

                     ]

            toBeAdded = 0
            zzCor = 18 * [0]
            for i in range(18):
                if i in range(3, 18, 3):
                    toBeAdded += zCor[i-1] * 1.25
                zzCor[i] = zCor[i] + toBeAdded
            yyCor = [yy - yCor[0] for yy in yCor]
            xCor = [sqrt(z**2 - y**2) for z, y in zip(zzCor, yyCor)]
            plt.scatter(xCor[0:-1:3], yCor[0:-1:3], color='r')
            plt.scatter(xCor[1:-1:3], yCor[1:-1:3], color='g')
            plt.scatter(xCor[2:18:3], yCor[2:18:3], color='b')
            plt.axhline(compressor.m_stageOne.m_rotor.m_tipRadius, color='black')

            plt.plot([xCor[6], xCor[6]], [rrCor[6], compressor.m_stageOne.m_rotor.m_tipRadius], 'red')
            plt.plot([xCor[7], xCor[7]], [rrCor[7], compressor.m_stageOne.m_rotor.m_tipRadius], 'red')
            plt.plot([xCor[8], xCor[8]], [rrCor[8], compressor.m_stageOne.m_rotor.m_tipRadius], 'red')
            plt.plot([xCor[6], xCor[7], xCor[8]], [rrCor[6], rrCor[7], rrCor[8]], 'red')
            plt.plot([xCor[9], xCor[9]], [rrCor[9], compressor.m_stageOne.m_rotor.m_tipRadius], 'blue')
            plt.plot([xCor[10], xCor[10]], [rrCor[10], compressor.m_stageOne.m_rotor.m_tipRadius], 'blue')
            plt.plot([xCor[11], xCor[11]], [rrCor[11], compressor.m_stageOne.m_rotor.m_tipRadius], 'blue')
            plt.plot([xCor[9], xCor[10], xCor[11]], [rrCor[9], rrCor[10], rrCor[11]], 'blue')

            plt.plot([xCor[12], xCor[12]], [rrCor[12], compressor.m_stageOne.m_rotor.m_tipRadius], 'red')
            plt.plot([xCor[13], xCor[13]], [rrCor[13], compressor.m_stageOne.m_rotor.m_tipRadius], 'red')
            plt.plot([xCor[14], xCor[14]], [rrCor[14], compressor.m_stageOne.m_rotor.m_tipRadius], 'red')
            plt.plot([xCor[12], xCor[13], xCor[14]], [rrCor[12], rrCor[13], rrCor[14]], 'red')
            plt.plot([xCor[15], xCor[15]], [rrCor[15], compressor.m_stageOne.m_rotor.m_tipRadius], 'blue')
            plt.plot([xCor[16], xCor[16]], [rrCor[16], compressor.m_stageOne.m_rotor.m_tipRadius], 'blue')
            plt.plot([xCor[17], xCor[17]], [rrCor[17], compressor.m_stageOne.m_rotor.m_tipRadius], 'blue')
            plt.plot([xCor[15], xCor[16], xCor[17]], [rrCor[15], rrCor[16], rrCor[17]], 'blue')

            plt.plot([xCor[0], xCor[0]], [rrCor[0], compressor.m_stageOne.m_rotor.m_tipRadius], 'red')
            plt.plot([xCor[1], xCor[1]], [rrCor[1], compressor.m_stageOne.m_rotor.m_tipRadius], 'red')
            plt.plot([xCor[2], xCor[2]], [rrCor[2], compressor.m_stageOne.m_rotor.m_tipRadius], 'red')
            plt.plot([xCor[0], xCor[1], xCor[2]], [rrCor[0], rrCor[1], rrCor[2]], 'red')
            plt.plot([xCor[3], xCor[3]], [rrCor[3], compressor.m_stageOne.m_rotor.m_tipRadius], 'blue')
            plt.plot([xCor[4], xCor[4]], [rrCor[4], compressor.m_stageOne.m_rotor.m_tipRadius], 'blue')
            plt.plot([xCor[5], xCor[5]], [rrCor[5], compressor.m_stageOne.m_rotor.m_tipRadius], 'blue')
            plt.plot([xCor[3], xCor[4], xCor[5]], [rrCor[3], rrCor[4], rrCor[5]], 'blue')
            plt.plot(xCor, yCor, 'black')
            plt.title('Multi-Stage Compressor Annulus Area')
            plt.axis('equal')


            plt.savefig('Output/AnnulusArea.jpg')


        drawAnnulusArea(compressor)
        drawTriangle(compressor.m_stageOne, 'Stage1')
        drawTriangle(compressor.m_stageTwo, 'Stage2')
        drawTriangle(compressor.m_stageThree, 'Stage3')
        print "This is fine"
    except None:
        print "This is totaly wrong"