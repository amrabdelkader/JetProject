from Libraries.GeneticAlgorthim.DataStructures.Chromosome import Chromosome
from Libraries.GeneticAlgorthim.DataStructures.Population import Population
from Libraries.GeneticAlgorthim.Models.Models import SimpleModel
from Libraries.GeneticAlgorthim.SelectionFunctions.SimpleFunctions import FitnessBasedSelection, RandomBasedSelection
from Libraries.GeneticAlgorthim.CrossoverFunctions.SimpleFunctions import RandomMultiPointCrossover
from Libraries.GeneticAlgorthim.common import SavePopulation
from DataStructures.ThreeStageCompressor import ThreeStageCompressor
from random import sample, randint
from csv import writer
import warnings


class StageModel(SimpleModel):

    def __init__(self):
        SimpleModel.__init__(self)
        self.m_threeStageCompressor = ThreeStageCompressor(27.5)
        self.m_threeStageCompressor.MapInletConditions(280.0, 10.0e4)
        self.counter = 0

    def InitializeParameters(self):
        # Generations Parameters.
        self.m_maxGenerationNumber = 1000

        # Population Parameters.
        self.m_populationSize = 10000
        self.m_fitnessBasedSurvivingRatio = 0.25
        self.m_randomBasedSurvivingRatio = 0.05
        self.m_mutationRatio = 0.15

        # Chromosome Parameters.
        self.m_genesCount = 21
        # Genes Parameters.
        self.m_upperConstrainsList = [0.523, 150, 0.4, 5, 0.7853, 0.7853, 0.7853, 0.7853, 0.7853, 0.75, 0.75, 0.75, 1.05, 1.05, 1.05, 0.45, 0.45, 0.45, 1.05, 1.05, 1.05]
        self.m_lowerConstrainsList = [0    , 120, 0.3, 1, 0.0   , 0.0   , 0.0   , 0.0   , 0.0   , 0.00, 0.00, 0.00, 1.00, 1.00, 1.00, 0.35, 0.35, 0.35, 1.00, 1.00, 1.00]
        self.m_genoTypesList = self.m_genesCount * ["Real"]
        assert len(self.m_upperConstrainsList) == len(self.m_lowerConstrainsList) == self.m_genesCount


class StageChromosome(Chromosome):

    def __init__(self, model):
        Chromosome.__init__(self, model)

    def FitnessFunction(self):
        warnings.catch_warnings()
        warnings.filterwarnings('error')
        got_error = 0
        try:
            self.m_model.m_threeStageCompressor.ChangeDesignParametersAndTest(self.m_genesList[0].m_allele,
                                                                              self.m_genesList[1].m_allele,
                                                                              self.m_genesList[2].m_allele,
                                                                              self.m_genesList[3].m_allele,
                                                                              self.m_genesList[4].m_allele,
                                                                              self.m_genesList[5].m_allele,
                                                                              self.m_genesList[6].m_allele,
                                                                              self.m_genesList[7].m_allele,
                                                                              self.m_genesList[8].m_allele,
                                                                              self.m_genesList[9].m_allele,
                                                                              self.m_genesList[10].m_allele,
                                                                              self.m_genesList[11].m_allele,
                                                                              self.m_genesList[12].m_allele,
                                                                              self.m_genesList[13].m_allele,
                                                                              self.m_genesList[14].m_allele,
                                                                              self.m_genesList[15].m_allele,
                                                                              self.m_genesList[16].m_allele,
                                                                              self.m_genesList[17].m_allele,
                                                                              self.m_genesList[18].m_allele,
                                                                              self.m_genesList[19].m_allele,
                                                                              self.m_genesList[20].m_allele,
                                                                              1, 1, 1, 1, 1, 1
                                                                              )
        except ValueError:
            got_error = 1
        except RuntimeError:
            got_error = 1
        except RuntimeWarning:
            got_error = 1
        except None:
            got_error = 1
        except:
            got_error = 1
        else:
            self.m_fitness = -self.m_model.m_threeStageCompressor.CalculateFitness()
            if self.m_model.m_threeStageCompressor.m_stageThree.m_stator.m_axialVelocityOutlet > 400 or self.m_model.m_threeStageCompressor.m_stageTwo.m_stator.m_axialVelocityOutlet > 400 or self.m_model.m_threeStageCompressor.m_stageOne.m_stator.m_axialVelocityOutlet > 400\
                    or self.m_model.m_threeStageCompressor.m_stageThree.m_rotor.m_axialVelocityOutlet > 400 or self.m_model.m_threeStageCompressor.m_stageTwo.m_rotor.m_axialVelocityOutlet > 400 or self.m_model.m_threeStageCompressor.m_stageOne.m_rotor.m_axialVelocityOutlet > 400:

                self.m_fitness = 10e10
            elif self.m_model.m_threeStageCompressor.m_stageThree.m_stator.m_axialVelocityOutlet > 150:
                self.m_fitness = 100 + self.m_fitness
        finally:
            if got_error:
                self.m_fitness = 1000000000000000



stageModel = StageModel()
stageModel.InitializeParameters()
populationList = []
population = Population(stageModel)
population.m_chromosomesList = [StageChromosome(stageModel) for _ in range(stageModel.m_populationSize)]
for chromosome in population.m_chromosomesList:
    chromosome.Initialize(stageModel)
population.RandomInitialization()
population.CalculateFitness()
populationList.append(population)
with open("output.csv", "w") as csvFile:

    csvWriter = writer(csvFile, dialect='excel')
    SavePopulation(csvWriter, population)
    for i in range(1, stageModel.m_maxGenerationNumber):
        print "generation number ##", i
        selectedChromosomes = FitnessBasedSelection(population)
        selectedChromosomes.extend(RandomBasedSelection(population))
        population = Population(stageModel)
        population.m_chromosomesList = selectedChromosomes
        ParentsCount = len(selectedChromosomes)
        neededChromosomesCount = stageModel.m_populationSize - len(selectedChromosomes)
        for _ in range(neededChromosomesCount):
            population.m_chromosomesList.append(StageChromosome(stageModel))
            RandomMultiPointCrossover(population.m_chromosomesList[-1],
                                      sample(population.m_chromosomesList[:ParentsCount],
                                             randint(2, min([ParentsCount, stageModel.m_genesCount]))))

        population.Mutate()
        population.CalculateFitness()
        SavePopulation(csvWriter, population)
        #populationList.append(deepcopy(population))
