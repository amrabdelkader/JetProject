from Libraries.GeneticAlgorthim.DataStructures.Chromosome import Chromosome
from Libraries.GeneticAlgorthim.DataStructures.Population import Population
from Libraries.GeneticAlgorthim.Models.Models import SimpleModel
from Libraries.GeneticAlgorthim.SelectionFunctions.SimpleFunctions import FitnessBasedSelection, RandomBasedSelection
from Libraries.GeneticAlgorthim.CrossoverFunctions.SimpleFunctions import RandomMultiPointCrossover
from Libraries.GeneticAlgorthim.common import SavePopulation
from DataStructures.Stage import Stage
from copy import deepcopy
from random import sample, randint
from csv import writer
import warnings


class StageModel(SimpleModel):

    def __init__(self):
        SimpleModel.__init__(self)
        self.m_stage = Stage(27.5)
        self.m_stage.MapInletConditions(280.0, 10.0e4)

    def InitializeParameters(self):
        # Generations Parameters.
        self.m_maxGenerationNumber = 100

        # Population Parameters.
        self.m_populationSize = 1000
        self.m_fitnessBasedSurvivingRatio = 0.25
        self.m_randomBasedSurvivingRatio = 0.05
        self.m_mutationRatio = 0.15

        # Chromosome Parameters.
        self.m_genesCount = 9

        # Genes Parameters.
        self.m_upperConstrainsList = [0.523, 1.05, 150, 0.75, 0.45, 0.8, 5, 1.05, 1.22]
        self.m_lowerConstrainsList = [0.0,      1, 120, 0.55, 0.35, 0.6, 1, 1   , 0]
        self.m_genoTypesList = self.m_genesCount * ["Real"]


class StageChromosome(Chromosome):

    def __init__(self, model):
        Chromosome.__init__(self, model)

    def FitnessFunction(self):
        self.m_model.m_stage.ChangeDesignParameters(alphaInlet=self.m_genesList[0].m_allele,
                                                    rotationalSpeedRatio=self.m_genesList[1].m_allele,
                                                    axialVelocityInlet=self.m_genesList[2].m_allele,
                                                    machNumberRelativeInlet=self.m_genesList[3].m_allele,
                                                    diffusionFactorRotor=self.m_genesList[4].m_allele,
                                                    zetaMean=self.m_genesList[5].m_allele,
                                                    sigmaMeanRotor=1,
                                                    sigmaMeanStator=1,
                                                    heightToChordRatio=self.m_genesList[6].m_allele,
                                                    alphaOutletStator=self.m_genesList[0].m_allele,
                                                    alphaOutletRotor=self.m_genesList[8].m_allele,
                                                    radiusRatio=self.m_genesList[7].m_allele)
        with warnings.catch_warnings():
            warnings.filterwarnings('error')
            try:
                self.m_model.m_stage.Test()
                self.m_fitness = -self.m_model.m_stage.CalculateFitness()

                if self.m_model.m_stage.m_stator.m_axialVelocityOutlet > 400 or self.m_model.m_stage.m_rotor.m_axialVelocityOutlet > 400:
                    self.m_fitness = 10e10
            except RuntimeWarning:
                self.m_fitness = 1000000000000000
            except RuntimeError:
                self.m_fitness = 1000000000000000
            print self.m_model.m_stage
            print self.m_fitness


stageModel = StageModel()
stageModel.InitializeParameters()
populationList = []
population = Population(stageModel)
population.m_chromosomesList = [StageChromosome(stageModel) for _ in range(stageModel.m_populationSize)]
for chromosome in population.m_chromosomesList:
    chromosome.Initialize(stageModel)
population.RandomInitialization()
population.CalculateFitness()
populationList.append(population)
with open("output.csv", "w") as csvFile:

    csvWriter = writer(csvFile, dialect='excel')
    SavePopulation(csvWriter, population)
    for i in range(1, stageModel.m_maxGenerationNumber):
        print "generation number ##", i
        selectedChromosomes = FitnessBasedSelection(population)
        selectedChromosomes.extend(RandomBasedSelection(population))
        population = Population(stageModel)
        population.m_chromosomesList = selectedChromosomes
        ParentsCount = len(selectedChromosomes)
        neededChromosomesCount = stageModel.m_populationSize - len(selectedChromosomes)
        for _ in range(neededChromosomesCount):
            population.m_chromosomesList.append(StageChromosome(stageModel))
            RandomMultiPointCrossover(population.m_chromosomesList[-1],
                                      sample(population.m_chromosomesList[:ParentsCount],
                                             randint(2, min([ParentsCount, stageModel.m_genesCount]))))

        population.Mutate()
        population.CalculateFitness()
        SavePopulation(csvWriter, population)
        populationList.append(deepcopy(population))
