from math import sqrt, cos, acos, sin, degrees, tan, pi
from scipy.optimize import newton
from Graphs import Graphs


class Rotor:
    # Design Parameters
    m_alphaInlet = 0
    m_rotationalSpeedRatio = 0
    m_axialVelocityInlet = 0
    m_axialVelocityOutlet = 0
    m_machNumberRelativeInlet = 0
    m_diffusionFactor = 0
    m_zetaMean = 0
    m_sigmaMean = 0
    m_meanHeightToChordRatio = 0

    m_machNumberInlet = 0
    m_massFlowParameter = 0
    m_temperatureInlet = 0
    m_pressureInlet = 0

    m_stagnationTemperatureInlet = 0
    m_stagnationRelativeTemperatureInlet = 0
    m_stagnationPressureInlet = 0
    m_stagnationRelativePressureInlet = 0

    m_temperatureOutlet = 0
    m_stagnationTemperatureOutlet = 0
    m_stagnationRelativeTemperatureOutlet = 0
    m_stagnationPressureOutlet = 0
    m_pressureOutlet = 0
    m_stagnationRelativePressureOutlet = 0
    m_stagnationRelativePressureOutletDash = 0

    m_inletAnnulusArea = 0
    m_outletAnnulusArea = 0
    m_absVelocityInlet = 0
    m_relativeVelocityInlet = 0
    m_betaInlet = 0
    m_cosBetaInlet = 0
    m_sinBetaInlet = 0
    m_tanBetaInlet = 0
    m_rotationalSpeedInlet = 0
    m_sinAlphaInlet = 0
    m_cosAlphaInlet = 0
    m_tanAlphaInlet = 0
    m_betaOutlet = 0
    m_cosBetaOutlet = 0
    m_sinBetaOutlet = 0
    m_tanBetaOutlet = 0
    m_cosAlphaOutlet = 0
    m_sinAlphaOutlet = 0
    m_tanAlphaOutlet = 0

    m_rotationalSpeedOutlet = 0
    m_relativeVelocityOutlet = 0
    m_relativeThetaVelocityOutlet = 0
    m_absVelocityOutlet = 0
    m_machNumberOutlet = 0
    m_betaDashOutlet = 0
    m_betaDashInlet = 0
    m_stagger = 0
    m_chord = 0

    m_pressureRatio = 0
    m_temperatureRatio = 0
    m_efficiency = 0
    m_meanRadius = 0
    m_meanHeight = 0
    m_soundSpeedInlet = 0

    m_i0 = 0
    m_betaInletDeg = 0
    m_betaOutletDeg = 0
    m_sinMeanRadiusLineAngle = 0
    m_previousChord = 0
    m_inletRadius = 0
    m_previousInletRadius = 0
    m_outletRadius = 0
    m_tipRadius = 0
    m_hubRadius = 0
    m_angularVelocity = 0

    m_inletZeta = 0
    m_inletHeight = 0
    m_outletZeta = 0
    m_outletHeight = 0

    m_meanAnnulusArea = 0

    def __init__(self, massFlowRate, gamma=1.4, gasConstant=287.0, cp=1005.0):
        # Graphs
        self.m_graphs = Graphs()
        # Inlet Conditions
        self.m_massFlowRate = massFlowRate
        self.m_gamma = gamma
        self.m_gasConstant = gasConstant
        self.m_cp = cp

        # Common Formulas
        self.m_gammaMinusOne = self.m_gamma - 1.0
        self.m_gammaPlusOne = self.m_gamma + 1.0
        self.m_isentropicPower = self.m_gamma / self.m_gammaMinusOne
        self.m_cp2 = self.m_cp * 2.0
        pass

    def MapInletConditions(self, temperatureInlet, pressureInlet):
        self.m_temperatureInlet = temperatureInlet
        self.m_pressureInlet = pressureInlet
        self.m_soundSpeedInlet = sqrt(self.m_gamma * self.m_gasConstant * self.m_temperatureInlet)

    def MapStatorInformation(self, stator):
        self.m_alphaInlet = stator.m_alphaOutlet
        self.m_cosAlphaInlet = stator.m_cosAlphaOutlet
        self.m_sinAlphaInlet = stator.m_sinAlphaOutlet
        self.m_tanAlphaInlet = stator.m_tanAlphaOutlet
        self.m_stagnationPressureInlet = stator.m_stagnationPressureOutlet
        self.m_stagnationTemperatureInlet = stator.m_stagnationTemperatureOutlet
        self.m_previousChord = stator.m_chord
        self.m_previousInletRadius = stator.m_inletRadius
        self.m_sinMeanRadiusLineAngle = stator.m_sinMeanRadiusLineAngle
        self.m_tipRadius = stator.m_tipRadius
        self.m_angularVelocity = stator.m_angularVelocity

    def ChangeDesignParameters(self, rotationalSpeedRatio=0.0, axialVelocityInlet=0.0, alphaOutlet=0.0,
                               machNumberRelativeInlet=0.0, diffusionFactor=0.0, sigmaMean=0.0,
                               alphaInlet=0.0, zetaMean=0.0, heightToChordRatio=0.0):
        # Design Parameters
        self.m_alphaInlet = alphaInlet
        self.m_rotationalSpeedRatio = rotationalSpeedRatio
        self.m_axialVelocityInlet = axialVelocityInlet
        self.m_alphaOutlet = alphaOutlet
        self.m_machNumberRelativeInlet = machNumberRelativeInlet
        self.m_diffusionFactor = diffusionFactor
        self.m_zetaMean = zetaMean
        self.m_sigmaMean = sigmaMean
        self.m_meanHeightToChordRatio = heightToChordRatio

    def CalculateStagnationIntelConditions(self):
        self.m_stagnationTemperatureInlet = self.m_temperatureInlet + self.m_absVelocityInlet**2 / self.m_cp2

        self.m_stagnationRelativeTemperatureInlet = self.m_temperatureInlet + self.m_relativeVelocityInlet ** 2 / self.m_cp2

        self.m_stagnationPressureInlet = \
            self.IsentropicRelation(self.m_pressureInlet, self.m_temperatureInlet, self.m_stagnationTemperatureInlet)

        self.m_stagnationRelativePressureInlet = \
            self.IsentropicRelation(self.m_pressureInlet,
                                    self.m_temperatureInlet, self.m_stagnationRelativeTemperatureInlet)

    def CalculateStagnationOutletConditions(self):
        lossesCoefficient = \
           2 * self.m_graphs.CalculateLosses(self.m_diffusionFactor) * 2.0 * self.m_sigmaMean / cos(self.m_cosBetaOutlet)

        self.m_stagnationRelativeTemperatureOutlet = \
            self.m_stagnationRelativeTemperatureInlet + \
            (self.m_rotationalSpeedOutlet**2 - self.m_rotationalSpeedInlet**2) / self.m_cp2

        self.m_temperatureOutlet = \
            self.m_stagnationRelativeTemperatureOutlet - self.m_relativeVelocityOutlet**2 / self.m_cp2

        self.m_stagnationTemperatureOutlet = self.m_temperatureOutlet + self.m_absVelocityOutlet**2 / self.m_cp2

        self.m_stagnationRelativePressureOutletDash = \
            self.IsentropicRelation(self.m_stagnationRelativePressureInlet, self.m_stagnationRelativeTemperatureInlet,
                                    self.m_stagnationRelativeTemperatureOutlet)

        self.m_stagnationRelativePressureOutlet = \
            self.m_stagnationRelativePressureOutletDash - \
            lossesCoefficient * (self.m_stagnationRelativePressureInlet - self.m_pressureInlet)

        self.m_stagnationPressureOutlet = \
            self.IsentropicRelation(self.m_stagnationRelativePressureOutlet,
                                    self.m_stagnationRelativeTemperatureOutlet, self.m_stagnationTemperatureOutlet)

        self.m_machNumberOutlet = self.CalculateMachNumber(self.m_absVelocityOutlet,  self.m_temperatureOutlet)
        self.m_pressureOutlet = \
            self.IsentropicRelation(self.m_stagnationPressureOutlet,
                                    self.m_stagnationTemperatureOutlet, self.m_temperatureOutlet)

    def CalculateChord(self):
        self.m_inletAnnulusArea = self.CalculateAnnulusAreaUsingMFP(self.m_stagnationTemperatureInlet,
                                                                    self.m_stagnationPressureInlet,
                                                                    self.m_machNumberInlet,
                                                                    self.m_cosAlphaInlet)
        self.m_outletAnnulusArea = self.CalculateAnnulusAreaUsingMFP(self.m_stagnationTemperatureOutlet,
                                                                     self.m_stagnationPressureOutlet,
                                                                     self.m_machNumberOutlet,
                                                                     self.m_cosAlphaOutlet)
        self.m_meanAnnulusArea = (self.m_inletAnnulusArea + self.m_outletAnnulusArea) / 2.0
        self.m_meanRadius = sqrt(self.m_meanAnnulusArea * (1.0 + self.m_zetaMean)/(4.0 * pi * (1.0 - self.m_zetaMean)))
        self.m_meanHeight = self.m_meanAnnulusArea / (2.0 * pi * self.m_meanRadius)
        self.m_chord = self.m_meanHeight / self.m_meanHeightToChordRatio
        self.m_inletRadius = 2.0 * self.m_meanRadius / (1 + self.m_rotationalSpeedRatio)
        self.m_outletRadius = 2 * self.m_meanRadius - self.m_inletRadius

    def CalculateInletVelocityTriangle(self):
        if self.m_meanHeightToChordRatio:
            self.m_relativeVelocityInlet = self.m_soundSpeedInlet * self.m_machNumberRelativeInlet
            self.m_cosBetaInlet = self.m_axialVelocityInlet / self.m_relativeVelocityInlet
            self.m_betaInlet = acos(self.m_cosBetaInlet)
            self.m_sinBetaInlet = sin(self.m_betaInlet)
            self.m_tanBetaInlet = self.m_sinBetaInlet / self.m_cosBetaInlet
            self.m_cosAlphaInlet = cos(self.m_alphaInlet)
            self.m_sinAlphaInlet = sin(self.m_alphaInlet)
            self.m_tanAlphaInlet = self.m_sinAlphaInlet / self.m_cosAlphaInlet
            self.m_absVelocityInlet = self.m_axialVelocityInlet / self.m_cosAlphaInlet
            self.m_rotationalSpeedInlet = \
                self.m_relativeVelocityInlet * self.m_sinBetaInlet + self.m_absVelocityInlet * self.m_sinAlphaInlet
            self.m_machNumberInlet = self.m_absVelocityInlet / self.m_soundSpeedInlet
        else:
            self.CalculateMeanHeightToChordRatio()
            self.CalculateInletConditions()
            self.m_rotationalSpeedInlet = self.m_angularVelocity * self.m_inletRadius
            relativeThetaVelocity = self.m_rotationalSpeedInlet - self.m_absVelocityInlet * self.m_sinAlphaInlet
            self.m_relativeVelocityInlet = sqrt(relativeThetaVelocity**2 + self.m_axialVelocityInlet**2)
            self.m_cosBetaInlet = self.m_axialVelocityInlet / self.m_relativeVelocityInlet
            self.m_betaInlet = acos(self.m_cosBetaInlet)
            self.m_sinBetaInlet = sin(self.m_betaInlet)
            self.m_tanBetaInlet = self.m_sinBetaInlet / self.m_cosBetaInlet
            self.m_stagnationRelativeTemperatureInlet = self.m_temperatureInlet + self.m_relativeVelocityInlet ** 2 / self.m_cp2
            self.m_stagnationRelativePressureInlet = \
                self.IsentropicRelation(self.m_pressureInlet,
                                        self.m_temperatureInlet, self.m_stagnationRelativeTemperatureInlet)

    def CalculateOutletVelocityTriangle(self):
            self.m_cosAlphaOutlet = cos(self.m_alphaOutlet)
            self.m_sinAlphaOutlet = sin(self.m_alphaOutlet)
            self.m_tanAlphaOutlet = self.m_sinAlphaOutlet / self.m_cosAlphaOutlet
            self.m_rotationalSpeedOutlet = self.m_rotationalSpeedInlet * self.m_rotationalSpeedRatio
            self.m_betaOutlet = self.FindBetaOutletUsingDiffusionFactor()
            self.m_sinBetaOutlet = sin(self.m_betaOutlet)
            self.m_cosBetaOutlet = cos(self.m_betaOutlet)
            self.m_tanBetaOutlet = self.m_sinBetaOutlet / self.m_cosBetaOutlet
            self.m_axialVelocityOutlet = self.m_rotationalSpeedOutlet / (self.m_tanAlphaOutlet + self.m_tanBetaOutlet)
            self.m_relativeVelocityOutlet = self.m_axialVelocityOutlet / self.m_cosBetaOutlet
            self.m_absVelocityOutlet = self.m_axialVelocityOutlet / self.m_cosAlphaOutlet

    def CalculateBladeAngles(self):
        self.m_betaInletDeg = degrees(self.m_betaInlet)
        self.m_betaOutletDeg = degrees(self.m_betaOutlet)
        self.m_i0 = self.m_graphs.CalculateI0(self.m_betaInletDeg)
        n = self.m_graphs.CalculateN(self.m_betaInletDeg)
        expression1 = (0.23 + self.m_betaOutletDeg / 500)/sqrt(self.m_sigmaMean)
        expression2 = (self.m_betaInletDeg - self.m_i0) / (1.0 + n)
        expression3 = n / (1.0+n)
        self.m_betaDashOutlet = \
            (self.m_betaOutletDeg - expression1 * expression2) / (1.0 - expression1*(expression3 - 1.0))
        self.m_betaDashInlet = expression2 + expression3 * self.m_betaDashOutlet
        self.m_stagger = (self.m_betaDashInlet + self.m_betaDashOutlet) / 2

    def CalculatePressureRatio(self):
        self.m_pressureRatio = self.m_stagnationPressureOutlet / self.m_stagnationPressureInlet
        self.m_temperatureRatio = self.m_stagnationTemperatureOutlet / self.m_stagnationTemperatureInlet
        self.m_efficiency = (self.m_pressureRatio ** (1.0/self.m_isentropicPower) - 1.0) / (self.m_temperatureRatio - 1)

    def CalculateMeanHeightToChordRatio(self):
        self.m_inletRadius = self.m_previousChord * self.m_sinMeanRadiusLineAngle * 1.25 + self.m_previousInletRadius
        self.m_outletRadius = self.m_rotationalSpeedRatio * self.m_inletRadius
        self.m_meanRadius = (self.m_outletRadius + self.m_inletRadius) / 2.0
        self.m_zetaMean = 2.0 * self.m_meanRadius / self.m_tipRadius - 1.0
        self.m_hubRadius = 2.0 * self.m_meanRadius / (1.0/self.m_zetaMean + 1.0)
        self.m_meanHeight = self.m_tipRadius - self.m_hubRadius
        self.m_chord = (self.m_outletRadius - self.m_inletRadius) / self.m_sinMeanRadiusLineAngle
        self.m_meanHeightToChordRatio = self.m_meanHeight / self.m_chord
        self.m_inletZeta = 2.0 * self.m_inletRadius / self.m_tipRadius - 1.0
        self.m_inletHeight = self.m_tipRadius - 2.0 * self.m_inletRadius / (1.0 / self.m_inletZeta + 1.0)
        self.m_inletAnnulusArea = 2 * pi * self.m_inletRadius * self.m_inletHeight
        self.m_outletZeta = 2.0 * self.m_outletRadius / self.m_tipRadius - 1.0
        self.m_outletHeight = self.m_tipRadius - 2.0 * self.m_outletRadius / (1.0 / self.m_outletZeta + 1.0)
        self.m_outletAnnulusArea = 2 * pi * self.m_outletRadius * self.m_outletHeight

    def CalculateInletConditions(self):
        self.m_machNumberInlet = \
            self.SolveForMachNumber(self.CalculateMFPUsingArea(self.m_stagnationTemperatureInlet,
                                                               self.m_stagnationPressureInlet,
                                                               self.m_inletAnnulusArea, self.m_cosAlphaInlet))
        self.m_temperatureInlet = \
            self.m_stagnationTemperatureInlet / (1.0 + self.m_gammaMinusOne / 2.0
                                                 * self.m_machNumberInlet ** 2)
        self.m_absVelocityInlet = \
            self.m_machNumberInlet * sqrt(self.m_gamma * self.m_gasConstant * self.m_temperatureInlet)

        self.m_axialVelocityInlet = self.m_absVelocityInlet * self.m_cosAlphaInlet
        self.m_pressureInlet = \
            self.IsentropicRelation(self.m_stagnationPressureInlet,
                                    self.m_stagnationTemperatureInlet, self.m_temperatureInlet)

    def SolveForMachNumber(self, MFP):
        def Equation(MachNumber):
            return sqrt(self.m_gamma/self.m_gasConstant) * MachNumber * (1 + self.m_gammaMinusOne/2.0 * MachNumber**2) ** (-self.m_gammaPlusOne/(2.0 * self.m_gammaMinusOne)) - MFP
        return newton(Equation, 0.25, tol=1e-8)

    def Test(self):
        if not self.m_meanHeightToChordRatio:
            self.CalculateInletVelocityTriangle()
            self.CalculateOutletVelocityTriangle()
            self.CalculateStagnationOutletConditions()
            self.CalculateBladeAngles()

        else:
            self.CalculateInletVelocityTriangle()
            self.CalculateStagnationIntelConditions()
            self.CalculateOutletVelocityTriangle()
            self.CalculateStagnationOutletConditions()
            self.CalculateBladeAngles()
            self.CalculateChord()
            self.m_hubRadius = 2.0 * self.m_meanRadius / (1.0/self.m_zetaMean + 1)
            self.m_tipRadius = 2.0 * self.m_meanRadius / (self.m_zetaMean + 1)
            self.m_sinMeanRadiusLineAngle = (self.m_outletRadius - self.m_inletRadius) / self.m_chord
            self.m_angularVelocity = self.m_rotationalSpeedInlet / self.m_inletRadius

            self.m_inletZeta = 2.0 * self.m_inletRadius / self.m_tipRadius - 1.0
            self.m_inletHeight = self.m_tipRadius - 2.0 * self.m_inletRadius / (1.0 / self.m_inletZeta + 1.0)
            self.m_inletAnnulusArea = 2 * pi * self.m_inletRadius * self.m_inletHeight
            self.m_outletZeta = 2.0 * self.m_outletRadius / self.m_tipRadius - 1.0
            self.m_outletHeight = self.m_tipRadius - 2.0 * self.m_outletRadius / (1.0 / self.m_outletZeta + 1.0)
            self.m_outletAnnulusArea = 2 * pi * self.m_outletRadius * self.m_outletHeight
            self.m_meanAnnulusArea = (self.m_inletAnnulusArea + self.m_outletAnnulusArea) / 2

        self.CalculatePressureRatio()

    def __str__(self):
        show = ('Inlet', 'Outlet', 'Static', 'Stagnation', 'Velocity', 'Temperature', 'Absolute', 'Pressure', 'Axial', 'Relative', 'Rotational', 'Angles', 'Alpha', 'Beta', 'Geometry', 'Area', 'Mean', 'Chord', 'Radius', 'Height', 'Stagger')
        return "                        -Design Parameters-                          \n" \
               "\n"\
               "Alpha Inlet           : %07.3f, Rotational Speed Ratio     : %0.4f, Zeta Mean             : %0.4f\n" \
               "Axial Velocity Outlet : %07.3f, Mach Number Relative Inlet : %0.4f, Diffusion Factor      : %0.4f\n" \
               "Axial Velocity Inlet  : %07.3f, Sigma Mean                 : %0.4f, Height to Chord Ratio : %0.4f\n" \
               "*********************************************************************\n" \
               "%-7s: %-11s: %-12s: %07.3f, %-9s: %011.3f\n" \
               "%8s %-11s: %-12s: %07.3f, %-9s: %011.3f\n" \
               "%8s %-11s: %-12s: %07.3f, %-9s: %011.3f\n" \
               "%-7s: %-11s: %-12s: %07.3f, %-9s: %011.3f\n" \
               "%8s %-11s: %-12s: %07.3f, %-9s: %011.3f\n" \
               "%8s %-11s: %-12s: %07.3f, %-9s: %011.3f\n" \
               "*********************************************************************\n"\
               "                        -Velocity Triangles-                         \n" \
               "\n"\
               "%-7s: %-11s: %-12s: %07.3f, %-9s: %07.3f, %-8s: %06.3f, %-11s: %06.3f\n" \
               "%8s %-11s: %-12s: %07.3f, %-9s: %07.3f\n"\
               "%-7s: %-11s: %-12s: %07.3f, %-9s: %07.3f, %-8s: %06.3f, %-11s: %06.3f\n" \
               "%8s %-11s: %-12s: %07.3f, %-9s: %07.3f\n" \
               "*********************************************************************\n" \
               "                        - Blade Geometry -                           \n" \
               "\n" \
               "%-7s: %-11s: %-6s: %0.5f, %-9s: %0.5f, %-8s: %0.5f, %-8s: %07.3f \n" \
               "%-7s: %-11s: %-6s: %0.5f, %-9s: %0.5f, %-8s: %0.5f, %-8s: %06.3f, %0.5f, %0.5f  \n" \
               "%-7s: %-11s: %-6s: %0.5f, %-9s: %0.5f, %-8s: %0.5f, %-8s: %07.3f " % \
               (degrees(self.m_alphaInlet), self.m_rotationalSpeedRatio, self.m_zetaMean,
                self.m_axialVelocityOutlet, self.m_machNumberRelativeInlet, self.m_diffusionFactor,
                self.m_axialVelocityInlet, self.m_sigmaMean, self.m_meanHeightToChordRatio,
                show[0],  show[2], show[5], self.m_temperatureInlet, show[7], self.m_pressureInlet, ' ',
                show[3], show[5], self.m_stagnationTemperatureInlet, show[7], self.m_stagnationPressureInlet, ' ',
                show[9], show[5], self.m_stagnationRelativeTemperatureInlet, show[7], self.m_stagnationRelativePressureInlet,
                show[1], show[2], show[5], self.m_temperatureOutlet, show[7], self.m_pressureOutlet, ' ',
                show[3], show[5], self.m_stagnationTemperatureOutlet, show[7], self.m_stagnationPressureOutlet, ' ',
                show[9], show[5], self.m_stagnationRelativeTemperatureOutlet, show[7],
                self.m_stagnationRelativePressureOutlet,
                show[0], show[4], show[6], self.m_absVelocityInlet, show[8], self.m_axialVelocityInlet, show[9], self.m_relativeVelocityInlet,
                show[10], self.m_rotationalSpeedInlet,  ' ',
                show[11], show[12], degrees(self.m_alphaInlet), show[13], degrees(self.m_betaInlet),
                show[1], show[4], show[6], self.m_absVelocityOutlet, show[8], self.m_axialVelocityOutlet, show[9],
                self.m_relativeVelocityOutlet,
                show[10], self.m_rotationalSpeedOutlet,  ' ',
                show[11], show[12], degrees(acos(self.m_cosAlphaOutlet)), show[13], degrees(self.m_betaOutlet),
                show[0], show[14], show[15], self.m_inletAnnulusArea, show[18], self.m_inletRadius, show[19], self.m_inletHeight, show[13], self.m_betaDashInlet,
                show[16], show[14], show[15], self.m_meanAnnulusArea, show[18], self.m_meanRadius, show[19], self.m_meanHeight, show[20], self.m_stagger, self.m_tipRadius, self.m_hubRadius,
                show[1], show[14], show[15], self.m_outletAnnulusArea, show[18], self.m_outletRadius, show[19], self.m_outletHeight, show[13], self.m_betaDashOutlet)

    def CalculateAnnulusAreaUsingMFP(self, stagnationTemperature, stagnationPressure, machNumber, cosAlpha):
        return \
            self.m_massFlowRate * sqrt(stagnationTemperature) / (self.CalculateMassFlowParameter(machNumber)
                                                                 * stagnationPressure * cosAlpha)

    def CalculateMassFlowParameter(self, machNumber):
        return sqrt(self.m_gamma / self.m_gasConstant) * machNumber * \
            (1.0 + self.m_gammaMinusOne/2.0 * machNumber**2)**(-self.m_gammaPlusOne/(2*self.m_gammaMinusOne))

    def CalculateMFPUsingArea(self, stagnationTemperature, stagnationPressure, Area, cosAlpha):
        return self.m_massFlowRate * sqrt(stagnationTemperature) / (Area * stagnationPressure * cosAlpha)

    def FindBetaOutletUsingDiffusionFactor(self):
        def DiffusionFactorEquation(BetaOutlet):
            return 1.0 - self.m_diffusionFactor - self.m_cosBetaInlet / cos(BetaOutlet) + \
                   (self.m_tanBetaInlet - self.m_rotationalSpeedRatio * tan(BetaOutlet)) / \
                   ((1.0 + self.m_rotationalSpeedRatio) * self.m_sigmaMean) * self.m_cosBetaInlet
        return newton(DiffusionFactorEquation, self.m_betaInlet, tol=1e-8)

    def IsentropicRelation(self, pressure1, temperature1, temperature2):
        return pressure1 * (temperature2 / temperature1) ** self.m_isentropicPower

    def CalculateMachNumber(self, velocity, temperature):
        return velocity / sqrt(self.m_gamma * self.m_gasConstant * temperature)
