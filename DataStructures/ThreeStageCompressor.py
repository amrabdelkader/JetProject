from Stage import Stage


class ThreeStageCompressor:
    m_temperatureInlet = 0
    m_pressureInlet = 0
    m_pressureRatio = 0
    m_efficiency = 0

    def __init__(self, massFlowRate, gamma=1.4, gasConstant=287, cp=1005):
        self.m_stageOne = Stage(massFlowRate, gamma=gamma, gasConstant=gasConstant, cp=cp)
        self.m_stageTwo = Stage(massFlowRate, gamma=gamma, gasConstant=gasConstant, cp=cp)
        self.m_stageThree = Stage(massFlowRate, gamma=gamma, gasConstant=gasConstant, cp=cp)

    def MapInletConditions(self, temperatureInlet, pressureInlet):
        self.m_temperatureInlet = temperatureInlet
        self.m_pressureInlet = pressureInlet
        self.m_stageOne.MapInletConditions(self.m_temperatureInlet, self.m_pressureInlet)

    def ChangeDesignParametersAndTest(self,
                                      alphaInlet,  axialVelocityInlet, zetaMean1, heightToChordRatio1,
                                      alphaOutletRotor1, alphaOutletStator1,
                                      alphaOutletRotor2, alphaOutletStator2, alphaOutletRotor3,
                                      machNumberRelativeInlet1, machNumberRelativeInlet2, machNumberRelativeInlet3,
                                      rotationalSpeedRatio1, rotationalSpeedRatio2, rotationalSpeedRatio3,
                                      diffusionFactor1, diffusionFactor2,  diffusionFactor3,
                                      radiusRatio1, radiusRatio2, radiusRatio3,
                                      sigmaMeanRotor1, sigmaMeanStator1, sigmaMeanRotor2, sigmaMeanStator2,
                                      sigmaMeanRotor3, sigmaMeanStator3):

        self.m_stageOne.ChangeDesignParameters(alphaInlet, rotationalSpeedRatio1, axialVelocityInlet,
                                               machNumberRelativeInlet1, diffusionFactor1, sigmaMeanRotor1,
                                               sigmaMeanStator1, alphaOutletRotor1, alphaOutletStator1, radiusRatio1,
                                               zetaMean=zetaMean1, heightToChordRatio=heightToChordRatio1)
        self.m_stageOne.Test()
        self.m_stageTwo.ChangeDesignParameters(alphaOutletStator1, rotationalSpeedRatio2, axialVelocityInlet,
                                               machNumberRelativeInlet2, diffusionFactor2, sigmaMeanRotor2,
                                               sigmaMeanStator2, alphaOutletRotor2, alphaOutletStator2, radiusRatio2)

        self.m_stageTwo.MapInletConditions(self.m_stageOne.m_temperatureOutlet, self.m_stageOne.m_pressureOutlet,
                                           self.m_stageOne.m_stator)
        self.m_stageTwo.Test()
        self.m_stageThree.ChangeDesignParameters(alphaOutletStator2, rotationalSpeedRatio3, axialVelocityInlet,
                                                 machNumberRelativeInlet3, diffusionFactor3, sigmaMeanRotor3,
                                                 sigmaMeanStator3, alphaOutletRotor3, 0, radiusRatio3)
        self.m_stageThree.MapInletConditions(self.m_stageTwo.m_temperatureOutlet, self.m_stageTwo.m_pressureOutlet,
                                             self.m_stageTwo.m_stator)
        self.m_stageThree.Test()
        self.CalculateFitness()

    def CalculateFitness(self):
        self.m_pressureRatio = self.m_stageThree.m_stator.m_stagnationPressureOutlet / self.m_stageOne.m_rotor.m_stagnationPressureInlet
        self.m_efficiency = (self.m_pressureRatio ** (1/self.m_stageOne.m_stator.m_isentropicPower) - 1.0) /\
                            (self.m_stageThree.m_stator.m_stagnationTemperatureOutlet / self.m_stageOne.m_rotor.m_stagnationTemperatureInlet - 1)


        return 0.8 * self.m_pressureRatio + 0.2 * self.m_efficiency if self.m_efficiency > 0.8 else -50 + self.m_pressureRatio

    def __str__(self):
        return "********************************************************************************************************\n" \
               "\t\t\t\t\t\t\t\t\t\t Stage One \n " \
               "--------------------------------------------------------------------------------------------------------\n" \
               "%s\n" \
               "********************************************************************************************************\n" \
               "\t\t\t\t\t\t\t\t\t\t Stage Two \n" \
               "--------------------------------------------------------------------------------------------------------\n" \
               "%s\n" \
               "********************************************************************************************************\n" \
               "\t\t\t\t\t\t\t\t\t\t Stage Three \n" \
               "--------------------------------------------------------------------------------------------------------\n" \
               "%s\n" \
               "Pressure Ratio %s  Efficieny %s\n" \
               "--------------------------------------------------------------------------------------------------------\n" % \
               (self.m_stageOne.__str__(), self.m_stageTwo.__str__(), self.m_stageThree.__str__(), self.m_pressureRatio, self.m_efficiency)
