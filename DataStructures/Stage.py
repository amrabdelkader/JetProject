from Rotor import Rotor
from Stator import Stator


class Stage:
    m_pressureRatio = 0
    m_efficiency = 0
    m_temperatureInlet = 0
    m_pressureInlet = 0
    m_temperatureOutlet = 0
    m_pressureOutlet = 0

    def __init__(self, massFlowRate, gamma=1.4, gasConstant=287, cp=1005):
        self.m_rotor = Rotor(massFlowRate, gamma=gamma, gasConstant=gasConstant, cp=cp)
        self.m_stator = Stator(massFlowRate, gamma=gamma, gasConstant=gasConstant, cp=cp)
        pass

    def MapInletConditions(self, temperatureInlet, pressureInlet, stator=0):
        self.m_temperatureInlet = temperatureInlet
        self.m_pressureInlet = pressureInlet
        if stator:
            self.m_rotor.MapStatorInformation(stator)
        else:
            self.m_rotor.MapInletConditions(temperatureInlet, pressureInlet)

    def ChangeDesignParameters(self, alphaInlet, rotationalSpeedRatio, axialVelocityInlet,
                               machNumberRelativeInlet, diffusionFactorRotor, sigmaMeanRotor, sigmaMeanStator,
                               alphaOutletRotor, alphaOutletStator, radiusRatio, heightToChordRatio=0, zetaMean=0):

        self.m_rotor.ChangeDesignParameters(rotationalSpeedRatio=rotationalSpeedRatio,
                                            axialVelocityInlet=axialVelocityInlet,
                                            alphaOutlet=alphaOutletRotor,
                                            machNumberRelativeInlet=machNumberRelativeInlet,
                                            diffusionFactor=diffusionFactorRotor,
                                            sigmaMean=sigmaMeanRotor,
                                            alphaInlet=alphaInlet,
                                            zetaMean=zetaMean,
                                            heightToChordRatio=heightToChordRatio)
        self.m_stator.ChangeDesignParameters(alphaOutletStator, radiusRatio, sigmaMeanStator)

    def Test(self):
        self.m_rotor.Test()
        self.m_stator.MapRotorInformation(self.m_rotor)
        self.m_stator.Test()
        self.m_temperatureOutlet = self.m_stator.m_temperatureOutlet
        self.m_pressureOutlet = self.m_stator.m_pressureOutlet
        self.CalculateFitness()

    def CalculateFitness(self):
        self.m_pressureRatio = self.m_stator.m_stagnationPressureOutlet/self.m_rotor.m_stagnationPressureInlet
        self.m_efficiency = \
            (self.m_pressureRatio ** (1 / self.m_rotor.m_isentropicPower) - 1) \
            / (self.m_stator.m_stagnationTemperatureOutlet / self.m_rotor.m_stagnationTemperatureInlet - 1)
        return self.m_pressureRatio if self.m_efficiency > 0.8 else -50+self.m_pressureRatio

    def __str__(self):
        return "\t\t\t\t\t\t\t\t\tRotor \n %s\n" \
               "--------------------------------------------------------------------------------------------------------\n" \
               "\t\t\t\t\t\t\t\t\tStator\n %s\n" \
               "PressureRatio: %s Efficiency: %s \n" % (self.m_rotor.__str__(), self.m_stator.__str__(), self.m_pressureRatio, self.m_efficiency)
