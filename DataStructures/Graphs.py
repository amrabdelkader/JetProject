from scipy.interpolate import interp1d


class Graphs:
    def __init__(self):
        lowerGraphX = [1.7922346040344320e-03,
                       8.4491288491665636e-02,
                       1.3958370118091190e-01,
                       2.5450661909432865e-01,
                       3.2191968397047799e-01,
                       3.9399831955703768e-01,
                       4.5839769972559918e-01,
                       5.2437644407246897e-01,
                       5.7050032446346588e-01,
                       6.2887233521685182e-01,
                       6.9351345480046078e-01]

        lowerGraphY = [4.7162829844756620e-03,
                       5.3922899915539377e-03,
                       4.5997300522207778e-03,
                       7.4863576743999126e-03,
                       8.9212537737154932e-03,
                       1.2590081074582909e-02,
                       1.5519444771922169e-02,
                       1.9939390969543235e-02,
                       2.5868050123574637e-02,
                       3.1786349016961209e-02,
                       4.2175100378612627e-02]

        upperGraphX = [3.3232508993331481e-03,
                       1.1824616881274980e-01,
                       2.1486941300709703e-01,
                       3.4087205411017496e-01,
                       4.3472335301198139e-01,
                       5.0573027719183183e-01,
                       6.0310291357282519e-01,
                       6.5872715297517515e-01,
                       7.0200656624911717e-01]

        upperGraphY = [4.7149879518950527e-03,
                       7.6016155740741883e-03,
                       1.2741599886514261e-02,
                       2.6808243777097365e-02,
                       4.0902083351873224e-02,
                       5.8745042247514360e-02,
                       8.7009128319322129e-02,
                       1.0262722124147575e-01,
                       1.1527191935854932e-01]

        i0GraphX = [20.3129057620451,
                    24.0161190773147,
                    29.5193984669718,
                    36.0239097085812,
                    41.4273348204339,
                    48.1320924324338,
                    56.0361770076498,
                    64.2387487867155,
                    69.1391378060743,
                    72.3387772140682]

        i0GraphY = [1.53722062442857,
                    1.84902002463704,
                    2.25753132771103,
                    2.76365601366562,
                    3.17228000174149,
                    3.69792736427219,
                    4.26171859994007,
                    4.78567568744254,
                    5.05662677446865,
                    5.21100522704747]

        nGraphX = [16.2587842627322,
                   20.2728343887749,
                   24.3787290574257,
                   28.1221746008647,
                   32.4125798622684,
                   36.5209390535294,
                   39.9913154917544,
                   43.6453810151955,
                   47.3945771113917,
                   51.5997098904813,
                   55.8122362374016,
                   59.2949352886778,
                   62.1363655567709,
                   65.0721048900823,
                   67.1853508776048,
                   69.7594625932411]

        nGraphY = [-0.0614358543443666,
                   -0.0711566356684489,
                   -0.0817752073719740,
                   -0.0942175258070725,
                   -0.107534162977707,
                   -0.120860188805904,
                   -0.133316590227346,
                   -0.147568572407674,
                   -0.166328283800341,
                   -0.185967008257324,
                   -0.213728095088324,
                   -0.239721767133127,
                   -0.261235875938279,
                   -0.286355229247546,
                   -0.307906892682950,
                   -0.335752477432015]
        self.m_lowerLossesGraph = interp1d(lowerGraphX, lowerGraphY, fill_value='extrapolate')
        self.m_upperLossesGraph = interp1d(upperGraphX, upperGraphY, fill_value='extrapolate')
        self.m_i0Graph = interp1d(i0GraphX, i0GraphY, fill_value='extrapolate')
        self.m_nGraph = interp1d(nGraphX, nGraphY, fill_value='extrapolate')

    def CalculateLosses(self, diffusionFactor):
        return 0.2 * self.m_upperLossesGraph(diffusionFactor) + 0.8 * self.m_lowerLossesGraph(diffusionFactor)

    def CalculateI0(self, betaInlet):
        return self.m_i0Graph(betaInlet)

    def CalculateN(self, betaInlet):
        return self.m_nGraph(betaInlet)
