from Graphs import Graphs
from math import sin, cos, sqrt, pi, radians, degrees
from scipy.optimize import newton


class Stator:

    m_diffusionFactor = 0
    m_pressureRatio = 0
    m_pressureInlet = 0
    m_temperatureInlet = 0
    m_stagnationPressureInlet = 0
    m_stagnationTemperatureInlet = 0
    m_absVelocityInlet = 0
    m_axialVelocityInlet = 0
    m_machNumberInlet = 0
    m_alphaInlet = 0
    m_alphaOutlet = 0
    m_cosAlphaInlet = 0
    m_sinAlphaInlet = 0
    m_tanAlphaInlet = 0
    m_cosAlphaOutlet = 0
    m_sinAlphaOutlet = 0
    m_tanAlphaOutlet = 0
    m_sigmaMean = 0
    m_zetaMean = 0
    m_meanHeightToChordRatio = 0
    m_temperatureOutlet = 0
    m_stagnationTemperatureOutlet = 0
    m_stagnationPressureOutlet = 0
    m_absVelocityOutlet = 0
    m_axialVelocityOutlet = 0
    m_machNumberOutlet = 0
    m_pressureOutlet = 0
    m_chord = 0
    m_stagnationPressureOutletDash = 0
    m_inletAnnulusArea = 0
    m_outletAnnulusArea = 0
    m_radiusInlet = 0
    m_radiusOutlet = 0
    m_temperatureRatio = 0
    m_meanHeight = 0
    m_meanRadius = 0

    m_sinMeanRadiusLineAngle = 0
    m_previousChord = 0
    m_inletRadius = 0
    m_previousInletRadius = 0
    m_outletRadius = 0
    m_tipRadius = 0
    m_hubRadius = 0
    m_angularVelocity = 0
    m_rotationalSpeedRatio = 0
    m_inletZeta = 0
    m_inletHeight = 0
    m_outletZeta = 0
    m_outletHeight = 0
    lossesCoefficient = 0

    def __init__(self, massFlowRate, gamma=1.4, gasConstant=287, cp=1005):

        self.m_graphs = Graphs()
        self.m_massFlowRate = massFlowRate
        self.m_gamma = gamma
        self.m_gasConstant = gasConstant
        self.m_cp = cp

        # Common Formulas
        self.m_gammaMinusOne = self.m_gamma - 1
        self.m_gammaPlusOne = self.m_gamma + 1
        self.m_isentropicPower = self.m_gamma / self.m_gammaMinusOne
        self.m_cp2 = self.m_cp * 2

    def ChangeDesignParameters(self, alphaOutlet, rotationalSpeedRatio, sigmaMean):
        self.m_alphaOutlet = alphaOutlet
        self.m_rotationalSpeedRatio = rotationalSpeedRatio
        self.m_sigmaMean = sigmaMean

        self.m_cosAlphaOutlet = cos(self.m_alphaOutlet)
        self.m_sinAlphaOutlet = sin(self.m_alphaOutlet)
        self.m_tanAlphaOutlet = self.m_sinAlphaOutlet / self.m_cosAlphaOutlet

    def MapRotorInformation(self, rotor):
        self.m_alphaInlet = rotor.m_alphaOutlet
        self.m_cosAlphaInlet = rotor.m_cosAlphaOutlet
        self.m_sinAlphaInlet = rotor.m_sinAlphaOutlet
        self.m_tanAlphaInlet = rotor.m_tanAlphaOutlet
        self.m_stagnationPressureInlet = rotor.m_stagnationPressureOutlet
        self.m_stagnationTemperatureInlet = rotor.m_stagnationTemperatureOutlet
        self.m_previousChord = rotor.m_chord
        self.m_previousInletRadius = rotor.m_inletRadius
        self.m_sinMeanRadiusLineAngle = rotor.m_sinMeanRadiusLineAngle
        self.m_tipRadius = rotor.m_tipRadius
        self.m_angularVelocity = rotor.m_angularVelocity

    def CalculateInletConditions(self):
        self.m_machNumberInlet = \
            self.SolveForMachNumber(self.CalculateMFPUsingArea(self.m_stagnationTemperatureInlet,
                                                               self.m_stagnationPressureInlet,
                                                               self.m_inletAnnulusArea, self.m_cosAlphaInlet))
        self.m_temperatureInlet = \
            self.m_stagnationTemperatureInlet / (1.0 + self.m_gammaMinusOne / 2.0
                                                 * self.m_machNumberInlet ** 2)
        self.m_absVelocityInlet = \
            self.m_machNumberInlet * sqrt(self.m_gamma * self.m_gasConstant * self.m_temperatureInlet)

        self.m_axialVelocityInlet = self.m_absVelocityInlet * self.m_cosAlphaInlet
        self.m_pressureInlet = \
            self.IsentropicRelation(self.m_stagnationPressureInlet,
                                    self.m_stagnationTemperatureInlet, self.m_temperatureInlet)

    def CalculateStagnationOutletConditions(self):
        self.CalculateInletConditions()
        self.CalculateDiffusionFactor()
        self.lossesCoefficient = \
            2.0 * self.m_graphs.CalculateLosses(self.m_diffusionFactor)*2.0*self.m_sigmaMean / cos(self.m_cosAlphaOutlet)

        self.m_stagnationTemperatureOutlet = self.m_stagnationTemperatureInlet
        self.m_stagnationPressureOutletDash = self.IsentropicRelation(self.m_stagnationPressureInlet,
                                                                      self.m_stagnationTemperatureInlet,
                                                                      self.m_stagnationTemperatureOutlet)

        self.m_stagnationPressureOutlet = \
            self.m_stagnationPressureOutletDash - \
            self.lossesCoefficient * (self.m_stagnationPressureInlet - self.m_pressureInlet)
        massFlowParameter = self.CalculateMFPUsingArea(self.m_stagnationTemperatureOutlet,
                                                       self.m_stagnationPressureOutlet,
                                                       self.m_outletAnnulusArea, self.m_cosAlphaOutlet)
        self.m_machNumberOutlet = self.SolveForMachNumber(massFlowParameter)

        self.m_temperatureOutlet = \
            self.m_stagnationTemperatureOutlet / (1.0 + self.m_gammaMinusOne/2.0
                                                  * self.m_machNumberOutlet**2)

        self.m_absVelocityOutlet = \
            self.m_machNumberOutlet * sqrt(self.m_gamma * self.m_gasConstant * self.m_temperatureOutlet)
        self.m_axialVelocityOutlet = self.m_absVelocityOutlet * self.m_cosAlphaOutlet
        self.m_pressureOutlet = \
            self.IsentropicRelation(self.m_stagnationPressureOutlet,
                                    self.m_stagnationTemperatureOutlet, self.m_temperatureOutlet)

    def CalculatePressureRatio(self):
        self.m_pressureRatio = self.m_stagnationPressureOutlet / self.m_stagnationPressureInlet
        
    def CalculateMeanHeightToChordRatio(self):
        self.m_inletRadius = self.m_previousChord * self.m_sinMeanRadiusLineAngle * 1.25 + self.m_previousInletRadius
        self.m_outletRadius = self.m_rotationalSpeedRatio * self.m_inletRadius
        self.m_meanRadius = (self.m_outletRadius + self.m_inletRadius) / 2.0
        self.m_zetaMean = 2.0 * self.m_meanRadius / self.m_tipRadius - 1.0
        self.m_hubRadius = 2.0 * self.m_meanRadius / (1.0/self.m_zetaMean + 1.0)
        self.m_meanHeight = self.m_tipRadius - self.m_hubRadius
        self.m_chord = (self.m_outletRadius - self.m_inletRadius) / self.m_sinMeanRadiusLineAngle
        self.m_meanHeightToChordRatio = self.m_meanHeight / self.m_chord
        self.m_inletZeta = 2.0 * self.m_inletRadius / self.m_tipRadius - 1.0
        self.m_inletHeight = self.m_tipRadius - 2.0 * self.m_inletRadius / (1.0 / self.m_inletZeta + 1.0)
        self.m_inletAnnulusArea = 2 * pi * self.m_inletRadius * self.m_inletHeight
        self.m_outletZeta = 2.0 * self.m_outletRadius / self.m_tipRadius - 1.0
        self.m_outletHeight = self.m_tipRadius - 2.0 * self.m_outletRadius / (1.0 / self.m_outletZeta + 1.0)
        self.m_outletAnnulusArea = 2 * pi * self.m_outletRadius * self.m_outletHeight

    def Test(self):
        self.CalculateMeanHeightToChordRatio()
        self.CalculateStagnationOutletConditions()
        self.CalculatePressureRatio()

    def __str__(self):
        show = ('Inlet', 'Outlet', 'Static', 'Stagnation', 'Velocity', 'Temperature', 'Absolute', 'Pressure', 'Axial', 'Relative', 'Rotational', 'Angles', 'Alpha', 'Beta', 'Geometry', 'Area', 'Mean', 'Chord', 'Radius', 'Height', 'Stagger')
        return "                        -Design Parameters-                          \n" \
               "\n"\
               "Alpha Inlet           : %07.3f,                                     Zeta Mean             : %0.4f\n" \
               "Axial Velocity Outlet : %07.3f,                                     Diffusion Factor      : %0.4f\n" \
               "Axial Velocity Inlet  : %07.3f, Sigma Mean                 : %0.4f, Height to Chord Ratio : %0.4f\n" \
               "*********************************************************************\n" \
               "%-7s: %-11s: %-12s: %07.3f, %-9s: %011.3f\n" \
               "%8s %-11s: %-12s: %07.3f, %-9s: %011.3f\n" \
               "%-7s: %-11s: %-12s: %07.3f, %-9s: %011.3f\n" \
               "%8s %-11s: %-12s: %07.3f, %-9s: %011.3f\n" \
               "\t\t\t\t PressureDash: %011.3f, Losses: %07.3f\n" \
               "*********************************************************************\n"\
               "                        -Velocity Triangles-                         \n" \
               "\n"\
               "%-7s: %-11s: %-12s: %07.3f, %-9s: %07.3f Mach: %0.5f\n" \
               "%8s %-11s: %-12s: %07.3f\n"\
               "%-7s: %-11s: %-12s: %07.3f, %-9s: %07.3f Mach: %0.5f\n" \
               "%8s %-11s: %-12s: %07.3f\n" \
               "*********************************************************************\n" \
               "                        - Blade Geometry -                           \n" \
               "\n" \
               "%-7s: %-11s: %-12s: %0.5f, %-9s: %0.5f, %-8s: %0.5f \n" \
               "%-7s: %-11s: %-12s: %0.5f, %-9s: %0.5f, %-8s: %0.5f, %-8s: %06.3f  \n" \
               "%-7s: %-11s: %-12s: %0.5f, %-9s: %0.5f, %-8s: %0.5f " % \
               (degrees(self.m_alphaInlet), self.m_zetaMean,
                self.m_axialVelocityOutlet, self.m_diffusionFactor,
                self.m_axialVelocityInlet, self.m_sigmaMean, self.m_meanHeightToChordRatio,
                show[0],  show[2], show[5], self.m_temperatureInlet, show[7], self.m_pressureInlet, ' ',
                show[3], show[5], self.m_stagnationTemperatureInlet, show[7], self.m_stagnationPressureInlet,
                show[1], show[2], show[5], self.m_temperatureOutlet, show[7], self.m_pressureOutlet, ' ',
                show[3], show[5], self.m_stagnationTemperatureOutlet, show[7], self.m_stagnationPressureOutlet, self.m_stagnationPressureOutletDash,self.lossesCoefficient,
                show[0], show[4], show[6], self.m_absVelocityInlet, show[8], self.m_axialVelocityInlet, self.m_machNumberInlet,' ',
                show[11], show[12], degrees(self.m_alphaInlet),
                show[1], show[4], show[6], self.m_absVelocityOutlet, show[8], self.m_axialVelocityOutlet, self.m_machNumberOutlet,' ',
                show[11], show[12], degrees(self.m_alphaOutlet),
                show[0], show[14], show[15], self.m_inletAnnulusArea, show[18], self.m_inletRadius, show[19], self.m_inletHeight,
                show[16], show[14], show[17], self.m_chord, show[18], self.m_meanRadius, show[19], self.m_meanHeight, show[20], 0,
                show[1], show[14], show[15], self.m_outletAnnulusArea, show[18], self.m_outletRadius, show[19], self.m_outletHeight)

    def CalculateMFPUsingArea(self, stagnationTemperature, stagnationPressure, Area, cosAlpha):
        return self.m_massFlowRate * sqrt(stagnationTemperature) / (Area * stagnationPressure * cosAlpha)

    def CalculateMassFlowParameter(self, machNumber):
        return sqrt(self.m_gamma / self.m_gasConstant) * machNumber * \
            (1.0 + self.m_gammaMinusOne/2.0 * machNumber**2)**(-self.m_gammaPlusOne/(2*self.m_gammaMinusOne))

    def IsentropicRelation(self, pressure1, temperature1, temperature2):
        return pressure1 * (temperature2 / temperature1) ** self.m_isentropicPower

    def CalculateMachNumber(self, velocity, temperature):
        return velocity / sqrt(self.m_gamma * self.m_gasConstant * temperature)
    
    def CalculateDiffusionFactor(self):
        self.m_diffusionFactor = 1.0 - self.m_cosAlphaInlet / self.m_cosAlphaOutlet + \
                                 abs(self.m_tanAlphaInlet - self.m_rotationalSpeedRatio * self.m_tanAlphaOutlet) / \
                                 ((1.0 + self.m_rotationalSpeedRatio) * self.m_sigmaMean) * self.m_cosAlphaInlet
        if self.m_diffusionFactor > 1:
            self.m_diffusionFactor = 0
        assert self.m_diffusionFactor < 1

    def FindRadiusRatioUsingDiffusionFactor(self):

        def DiffusionFactorEquation(radiusRatio):
            return 1.0 - self.m_diffusionFactor - self.m_cosAlphaInlet / self.m_cosAlphaOutlet + \
                   abs(self.m_tanAlphaInlet - radiusRatio * self.m_tanAlphaOutlet) / \
                   ((1.0 + radiusRatio) * self.m_sigmaMean) * self.m_cosAlphaInlet
        return newton(DiffusionFactorEquation, 1, tol=1e-3)

    def SolveForMachNumber(self, MFP):
        def Equation(MachNumber):
            return sqrt(self.m_gamma/self.m_gasConstant) * MachNumber * (1 + self.m_gammaMinusOne/2.0 * MachNumber**2) ** -(self.m_gammaPlusOne/(2.0 * self.m_gammaMinusOne)) - MFP
        return newton(Equation, 0.25, tol=1e-8)
