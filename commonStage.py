from csv import reader
from DataStructures.Stage import Stage
from math import sqrt
import matplotlib.pyplot as plt
import warnings

with open('toBeRead.csv', 'r') as readFile:
    csvReader = reader(readFile)
    designParameters = csvReader.next()
designParameters = [float(parameter) for parameter in designParameters]
stage = Stage(27.5)
stage.MapInletConditions(280.0, 10.0e4)

with warnings.catch_warnings():
    warnings.filterwarnings('error')
    try:
        stage.ChangeDesignParameters(alphaInlet=designParameters[0],
                                     rotationalSpeedRatio=designParameters[1],
                                     axialVelocityInlet=designParameters[2],
                                     machNumberRelativeInlet=designParameters[3],
                                     diffusionFactorRotor=designParameters[4],
                                     zetaMean=designParameters[5],
                                     sigmaMeanRotor=1,
                                     sigmaMeanStator=1,
                                     heightToChordRatio=designParameters[6],
                                     alphaOutletStator=designParameters[0],
                                     alphaOutletRotor=designParameters[8],
                                     radiusRatio=designParameters[7])

        stage.Test()
        stage.CalculateFitness()
        outputFile = open('Output/Output.txt', 'w')
        outputFile.write(stage.__str__())


        def drawTriangle(Stage, StageName):
            C1 = Stage.m_rotor.m_absVelocityInlet
            C2 = Stage.m_rotor.m_absVelocityOutlet
            W1 = Stage.m_rotor.m_relativeVelocityInlet
            W2 = Stage.m_rotor.m_relativeVelocityOutlet
            Cx1 = Stage.m_rotor.m_axialVelocityInlet
            Cx2 = Stage.m_rotor.m_axialVelocityOutlet
            Ctheta1 = sqrt(C1**2 - Cx1 ** 2)
            Wtheta1 = sqrt(W1**2 - Cx1 ** 2)
            Ctheta2 = sqrt(C2**2 - Cx2 ** 2)
            Wtheta2 = sqrt(W2**2 - Cx2 ** 2)
            plt.figure()
            plt.plot([0, 0], [0, -Cx1], 'r')
            plt.plot([0, Ctheta1], [-Cx1, -Cx1], 'r')
            plt.plot([0, Ctheta1], [0, -Cx1], 'r')
            plt.plot([0, -Wtheta1], [-Cx1, -Cx1], 'r')
            plt.plot([0, -Wtheta1], [0, -Cx1], 'r')
            plt.plot([0, 0], [0, -Cx2], 'b')
            plt.plot([0, Ctheta2], [-Cx2, -Cx2], 'b')
            plt.plot([0, Ctheta2], [0, -Cx2], 'b')
            plt.plot([0, -Wtheta2], [-Cx2, -Cx2], 'b')
            plt.plot([0, -Wtheta2], [0, -Cx2], 'b')
            plt.title('Single Stage')
            plt.axis('equal')

            plt.savefig('Output/'+StageName+'.jpg')

        def drawAnnulusArea(stage):
            yCor = [stage.m_rotor.m_inletRadius,
                    stage.m_rotor.m_meanRadius,
                    stage.m_rotor.m_outletRadius,
                    stage.m_stator.m_inletRadius,
                    stage.m_stator.m_meanRadius,
                    stage.m_stator.m_outletRadius,
                    ]

            zCor = [0,
                    stage.m_rotor.m_chord/2,
                    stage.m_rotor.m_chord,
                    0,
                    stage.m_stator.m_chord/2,
                    stage.m_stator.m_chord,
                    ]
            rrCor = [stage.m_rotor.m_tipRadius - stage.m_rotor.m_inletHeight,
                     stage.m_rotor.m_tipRadius - stage.m_rotor.m_meanHeight,
                     stage.m_rotor.m_tipRadius - stage.m_rotor.m_outletHeight,
                     stage.m_stator.m_tipRadius - stage.m_stator.m_inletHeight,
                     stage.m_stator.m_tipRadius - stage.m_stator.m_meanHeight,
                     stage.m_stator.m_tipRadius - stage.m_stator.m_outletHeight
                     ]
            toBeAdded = 0
            zzCor = 6 * [0]
            for i in range(6):
                if i in range(3, 6, 3):
                    toBeAdded += zCor[i-1] * 1.25
                zzCor[i] = zCor[i] + toBeAdded
            yyCor = [yy - yCor[0] for yy in yCor]
            xCor = [sqrt(z**2 - y**2) for z, y in zip(zzCor, yyCor)]
            plt.scatter(xCor[0:-1:3], yCor[0:-1:3], color='r')
            plt.scatter(xCor[1:-1:3], yCor[1:-1:3], color='g')
            plt.scatter(xCor[2:6:3], yCor[2:6:3], color='b')
            plt.plot([xCor[0], xCor[0]], [rrCor[0], stage.m_rotor.m_tipRadius], 'black')
            plt.plot([xCor[1], xCor[1]], [rrCor[1], stage.m_rotor.m_tipRadius], 'black')
            plt.plot([xCor[2], xCor[2]], [rrCor[2], stage.m_rotor.m_tipRadius], 'black')
            plt.plot([xCor[0], xCor[1], xCor[2]], [rrCor[0], rrCor[1], rrCor[2]], 'black')
            plt.plot([xCor[3], xCor[3]], [rrCor[3], stage.m_rotor.m_tipRadius], 'black')
            plt.plot([xCor[4], xCor[4]], [rrCor[4], stage.m_rotor.m_tipRadius], 'black')
            plt.plot([xCor[5], xCor[5]], [rrCor[5], stage.m_rotor.m_tipRadius], 'black')
            plt.plot([xCor[3], xCor[4], xCor[5]], [rrCor[3], rrCor[4], rrCor[5]], 'black')

            plt.axhline(stage.m_rotor.m_tipRadius, color='black')
            plt.title('Single-Stage Compressor Annulus Area')
            plt.axis('equal')
            plt.savefig('Output/AnnulusArea.jpg')


        drawAnnulusArea(stage)
        drawTriangle(stage, 'Stage1')
        print stage
        print "This is fine"
    except None:
        print "This is totaly wrong"