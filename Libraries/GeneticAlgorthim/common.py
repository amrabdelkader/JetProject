def SavePopulation(csvWriter, population):
    chromosome = min(population.m_chromosomesList)
    listTobeWritten = [gene.m_allele for gene in chromosome.m_genesList] + [chromosome.m_fitness]
    csvWriter.writerow(listTobeWritten)
