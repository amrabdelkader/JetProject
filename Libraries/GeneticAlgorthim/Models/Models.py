class SimpleModel:

    def __init__(self):
        # Generations Parameters.
        self.m_maxGenerationNumber = 0

        # Population Parameters.
        self.m_populationSize = 0
        self.m_fitnessBasedSurvivingRatio = 0.0
        self.m_randomBasedSurvivingRatio = 0.0
        self.m_mutationRatio = 0.0

        # Chromosome Parameters.
        self.m_genesCount = 0

        # Genes Parameters.
        self.m_upperConstrainsList = []
        self.m_lowerConstrainsList = []
        self.m_genoTypesList = []

    def Update(self):
        pass
