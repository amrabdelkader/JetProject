from random import sample


def FitnessBasedSelection(population):
    selectedChromosomesList = []
    population.m_chromosomesList.sort()
    selectedCount = int(round(population.m_size * population.m_fitnessBasedSurvivingRatio))
    for chromosome in population.m_chromosomesList[:selectedCount]:
        selectedChromosomesList.append(chromosome)
    population.m_chromosomesList = population.m_chromosomesList[selectedCount:]
    population.m_size = len(population.m_chromosomesList)
    return selectedChromosomesList


def RandomBasedSelection(population):
    selectedCount = int(round(population.m_size * population.m_fitnessBasedSurvivingRatio))
    selectedChromosomesIndexList = sorted(sample(range(population.m_size), selectedCount), reverse=True)
    selectedChromosomesList = \
        [population.m_chromosomesList[index] for index in selectedChromosomesIndexList]
    for index in selectedChromosomesIndexList:
        del population.m_chromosomesList[index]
        # TODO check for whether deepcopy is needed or not
    population.m_size = len(population.m_chromosomesList)
    return selectedChromosomesList
