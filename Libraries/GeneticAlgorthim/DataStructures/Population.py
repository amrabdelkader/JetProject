from random import random


class Population:

    def __init__(self, model):
        self.m_size = model.m_populationSize
        self.m_generationNumber = 0
        self.m_fitnessBasedSurvivingRatio = model.m_fitnessBasedSurvivingRatio
        self.m_randomBasedSurvivingRatio = model.m_randomBasedSurvivingRatio
        self.m_mutationRatio = model.m_mutationRatio
        self.m_chromosomesList = []

    def RandomInitialization(self):
        for chromosome in self.m_chromosomesList:
            for gene in chromosome.m_genesList:
                gene.RandomInitialization()

    def Mutate(self):
        mutatedChromosomesIndices = [index for index in range(self.m_size)
                                     if random() <= self.m_mutationRatio]
        for index in mutatedChromosomesIndices:
            self.m_chromosomesList[index].RandomGeneMutate()

    def CalculateFitness(self):
        for chromosome in self.m_chromosomesList:
            if not chromosome.m_isFitnessCalculated or chromosome.m_isMutated:
                chromosome.m_isMutated = 0
                chromosome.m_isFitnessCalculated = 1
                chromosome.FitnessFunction()
