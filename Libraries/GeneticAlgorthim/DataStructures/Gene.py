from random import randint, uniform


class Gene:

    def __init__(self, upperConstrain, lowerConstrain, genoType):
        self.m_allele = 0
        self.m_upperConstrain = upperConstrain
        self.m_lowerConstrain = lowerConstrain
        self.m_genoType = genoType

    def RandomInitialization(self):
        if self.m_genoType == "Integer":
            self.m_allele = randint(self.m_lowerConstrain, self.m_upperConstrain)
        elif self.m_genoType == "Real":
            self.m_allele = uniform(self.m_lowerConstrain, self.m_upperConstrain)
        else:
            raise ValueError("Unexpected Genotype")

    def RandomResettingMutate(self):
        self.RandomInitialization()
