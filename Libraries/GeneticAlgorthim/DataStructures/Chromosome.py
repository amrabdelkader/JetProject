from Gene import Gene
from random import choice


class Chromosome:

    def __init__(self, model):
        self.m_genesCount = model.m_genesCount
        self.m_age = 0
        self.m_genesList = []
        self.m_fitness = 0
        self.m_isFitnessCalculated = 0
        self.m_isMutated = 0
        self.m_model = model

    def __cmp__(self, other):
        return cmp(self.m_fitness, other.m_fitness)

    def Initialize(self, model):
        self.m_genesList = [Gene(model.m_upperConstrainsList[geneIndex], model.m_lowerConstrainsList[geneIndex],
                                 model.m_genoTypesList[geneIndex])
                            for geneIndex in range(self.m_genesCount)]

    def RandomGeneMutate(self):
        self.m_isMutated = 1
        GeneToMutate = choice(self.m_genesList)
        GeneToMutate.RandomResettingMutate()

    def FitnessFunction(self):
        self.m_isFitnessCalculated = 1
        self.m_fitness = 0
