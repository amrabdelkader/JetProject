from DataStructures.Chromosome import Chromosome
from DataStructures.Population import Population
from Models.Models import SimpleModel
from SelectionFunctions.SimpleFunctions import FitnessBasedSelection, RandomBasedSelection
from CrossoverFunctions.SimpleFunctions import RandomMultiPointCrossover
from common import SavePopulation
from copy import deepcopy
from random import sample, randint


class ExampleModel(SimpleModel):

    def __init__(self):
        SimpleModel.__init__(self)

    def InitializeParameters(self):
        # Generations Parameters.
        self.m_maxGenerationNumber = 1000

        # Population Parameters.
        self.m_populationSize = 1000
        self.m_fitnessBasedSurvivingRatio = 0.25
        self.m_randomBasedSurvivingRatio = 0.05
        self.m_mutationRatio = 0.15

        # Chromosome Parameters.
        self.m_genesCount = 10

        # Genes Parameters.
        self.m_upperConstrainsList = [1000, 100, 400, 200, 300, 400, 900, 200, 400, 150]
        self.m_lowerConstrainsList = [-1000, -100, -400, -200, -300, -400, -900, -200, -400, -150]
        self.m_genoTypesList = 10 * ["Integer"]


class ExampleChromosome(Chromosome):

    def __init__(self, model):
        Chromosome.__init__(self, model)

    def FitnessFunction(self):
        self.m_fitness = sum([gene.m_allele*gene.m_allele for gene in self.m_genesList])


exampleModel = ExampleModel()
exampleModel.InitializeParameters()
populationList = []
population = Population(exampleModel)
population.m_chromosomesList = [ExampleChromosome(exampleModel) for _ in range(exampleModel.m_populationSize)]
for chromosome in population.m_chromosomesList:
    chromosome.Initialize(exampleModel)
population.RandomInitialization()
population.CalculateFitness()
populationList.append(population)
for i in range(1, exampleModel.m_maxGenerationNumber):
    print "generation number ##", i
    selectedChromosomes = FitnessBasedSelection(population)
    selectedChromosomes.extend(RandomBasedSelection(population))
    population = Population(exampleModel)
    population.m_chromosomesList = selectedChromosomes
    ParentsCount = len(selectedChromosomes)
    neededChromosomesCount = exampleModel.m_populationSize - len(selectedChromosomes)
    for _ in range(neededChromosomesCount):
        population.m_chromosomesList.append(ExampleChromosome(exampleModel))
        RandomMultiPointCrossover(population.m_chromosomesList[-1],
                                  sample(population.m_chromosomesList[:ParentsCount],
                                         randint(2, min([ParentsCount, exampleModel.m_genesCount]))))

    population.Mutate()
    population.CalculateFitness()
    SavePopulation("output.csv", population)
    populationList.append(deepcopy(population))
