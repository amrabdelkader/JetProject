from random import randint, sample
from itertools import cycle
from copy import deepcopy


def RandomMultiPointCrossover(chromosome, parentList):
    crossoverPointsCount = randint(1, chromosome.m_genesCount)
    crossoverPoints = sorted(sample(range(1, chromosome.m_genesCount + 1), crossoverPointsCount))
    previousCrossoverPoint = 0
    for crossoverPoint, parent in zip(crossoverPoints, cycle(parentList)):
        chromosome.m_genesList[previousCrossoverPoint: crossoverPoint] = \
            deepcopy(parent.m_genesList[previousCrossoverPoint: crossoverPoint])
        previousCrossoverPoint = crossoverPoint
    chromosome.m_genesList[crossoverPoints[-1]:chromosome.m_genesCount] = \
        deepcopy(parentList[-1].m_genesList[crossoverPoints[-1]:])
